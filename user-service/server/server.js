const express = require('express')
const apiRouter = require('./routes')//index hoeft niet want is defaulted
const app = express()
const cors = require('cors')

app.use(express.json())
app.use(
    cors({ 
        origin: 'http://localhost:4200', 
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        allowedHeaders: [
            'Content-Type', 
            'Authorization', 
            'Origin', 
            'x-access-token', 
            'XSRF-TOKEN'
        ], 
        preflightContinue: false 
    })
);
app.use('/api/users', apiRouter)

const db = require("../../user-service/models")



db.sequelize.sync().then((req) => {
    app.listen(process.env.PORT || '3000', () => {
        console.log(`listening on portx: ${process.env.PORT || '3000'}`)
    })
}).catch(err => {
    console.error(err)
})
