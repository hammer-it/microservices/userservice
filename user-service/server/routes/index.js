const express = require('express')
//const db = require('../db')
const router = express.Router();

const { User } = require("../../models")

router.get('/', async (req, res, next) => {
    User.findAll().then((users) => {
        res.send(users)
    }).catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
})


router.get('/:companyId', async (req, res, next) => {
    const id = req.params.companyId
    User.findAll({ where: { companyId: id } }).then((users) => {
        res.status(200).send(users)
    }).catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
})

router.put('/:userId', async (req, res, next) => {
    const uid = req.params.userId
    const user = req.body.user;

    User.update(user, { where: { id: uid } } ).then((user) => {
        console.log(user);
        res.status(200).send(user)
    }).catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
})

router.post('/', async (req, res, next) => {
    var newUser = req.body.user;

    User.create({
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        email: newUser.email,
        companyId: newUser.companyId,
        role: newUser.role
    }).then(() => {
        res.send(200)
    }).catch((err) => {
        console.log(err);
        res.sendStatus(500)
    })
})

module.exports = router