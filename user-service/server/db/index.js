// const mysql = require('mysql2');
// const { v4: uuidv4 } = require('uuid');
// const db = require('/models')

// //a pool will allow us to make calls to the pool like queries and it will always create a connection for us to know manage a list of connections
// const pool = mysql.createPool({
//     connectionLimit: 10,
//     password: 'test123',
//     user: 'root',
//     database: 'test_db',
//     host: 'mysql-app',
//     //host: 'localhost',
//     port: '3306'
// })

// let userdb = {}

// userdb.all = () => {
//     return new Promise((resolve, reject) => {
//         pool.query(`SELECT * FROM users`, (err, results) => {
//             if (err) {
//                 return reject(err)
//             }
//             return resolve(results)
//         })
//     })
// }

// userdb.one = (id) => {
//     return new Promise((resolve, reject) => {
//         pool.query(`SELECT * FROM users WHERE id = ?`, [id], (err, results) => {
//             if (err) {
//                 return reject(err)
//             }

//             return resolve(results[0])
//         })
//     })
// }

// userdb.post = (user) => {
//     //  console.log(user);
//     const newuser = user;
//     newuser.id = uuidv4();
//     newuser.created_at = new Date();

//     return new Promise((resolve, reject) => {
//         pool.query(`INSERT INTO users SET ?`, [newuser], (err, results) => {
//             if (err) {
//                 return reject(err)
//             }

//             return resolve(results[0])
//         })
//     })
// }

// module.exports = userdb